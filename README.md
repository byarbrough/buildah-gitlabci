# buildah-gitlabci

This repository is ready to demo for gitlab.com! It builds [the official Podman Dockerfile](https://github.com/containers/podman/tree/13f6261/contrib/podmanimage/stable) using [buildah](https://buildah.io/) inside GitLab docker runners.

## Medium Post

Detailed walkthrough: [Build Dockerfiles in GitLab CI shared runners the easy way: ditch dind](https://b-yarbrough.medium.com/build-dockerfiles-in-gitlab-ci-shared-runners-the-easy-way-ditch-dind-d3b39d9e8539)

## Usage

It all happens in CI! But if you have buildah installed, you can build locally with:

```bash
buildah bud -t podman:mytag
```
